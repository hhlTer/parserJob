package com.sb.parser.model.enums;



public enum Status {
    NOT_STARTED("not started"), IN_PROGRESS("in progress"), FINISHED("finished");

    private String name;

    public String getName() {
        return name;
    }

    private Status(String name){
        this.name = name;
    }
}
